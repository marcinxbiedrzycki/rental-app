<?php

namespace Tests\Feature;

use Illuminate\Validation\ValidationException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SubmitItemTest extends TestCase
{

    use RefreshDatabase;


    public function testExample()
    {
        $this->assertTrue(true);
    }

    /** @test */
    function guest_can_submit_a_new_item()
    {
        $response = $this->post('/submit', [
            'title' => 'Example Title',
            'description' => 'Example description',
        ]);

//        $this->assertDatabaseHas('items', [
//            'title' => 'Example Title'
//        ]);

        $response
            ->assertStatus(302)
            ->assertHeader('Location', url('/'));

        $this->get('/')->assertSee('Example Title');
    }

    /** @test */
    function item_is_not_created_if_validation_fails()
    {
        $response = $this->post('/submit');

        $response->assertSessionHasErrors(['title', 'description']);
    }

    /** @test */
    public function max_length_fails_when_too_long()
    {
        $this->withExceptionHandling();

        $title = str_repeat('a', 256);
        $description = str_repeat('a',256);

        try {
            $this->post('/submit', compact('title', 'description'));
           } catch (ValidationException $exception) {
        $this->assertEquals('The description may not be greater than 255 characters.', $exception->errors()->first('description'));

        return;
        }
        $this->fail('Max length should trigger a ValidationException');
    }

    /** @test */
    public function max_length_succeeds_when_under_max()
    {
        $data = [
            'title' => str_repeat('a', 255),
            'description' => str_repeat('a', 255),
        ];

        $this->post('/submit', $data);

        $this->assertDatabaseHas('items', $data);
    }
}
