<div class="form-group">
    {{--<label for="title">Title</label>--}}
    <label for="title">Avaliable quantity : {{$item->quantity}}</label>

    @if ($errors->any())
        <ul>{!! implode('', $errors->all('<li style="color:red">:message</li>')) !!}</ul>
    @endif

    <input type="number" class="form-control" name="rent_quantity" id="quantity">
    <input type="number" class="form-control" name="rent_item" id="item" value="{{$item->id}}" hidden>
</div>

