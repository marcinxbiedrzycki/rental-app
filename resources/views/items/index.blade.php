


@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="container-fluid" style="border:1px solid #cecece;">


        {{--<div class="row">--}}
            <br>
        <div class="d-flex align-content-around flex-wrap">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="row">

                        @foreach($items as $item)
                            <div>
                            <img src="{{ $item->image }}" alt="Any alt text"/>
                            <div class="caption">
                                {{--<h3><a href="{{ route('items.update', $item->id) }}">{{ $item->name }}</a></h3>--}}
                                {{--<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores--}}
                                    {{--error eum inventore officia quis quossadasdasd totam! Asperiores deleniti, distinctio illum--}}
                                    {{--incidunt nulla officiis sssssssssssssssssssquas suscipit vitae? Magni necessitatibus repellendus--}}
                                    {{--voluptate!</p>--}}
                                <p>{{$item->description}}</p>
                                <div class="clearfix">
                                    <div class="pull-left price">{{$item->price}} $</div>
                                    <a href="{{ route('items.update', $item->id) }}" class="btn btn-success pull-right"
                                       role="button">Detail</a>
                                </div>
                                    @endforeach
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
