@extends('layouts.app')

@section('content')
    <div class="container">
        {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
        {{--<div class="card">--}}
        {{--<div class="card-header">Dashboard</div>--}}

        {{--<div class="card-body">--}}
        {{--@if (session('status'))--}}
        {{--<div class="alert alert-success" role="alert">--}}
        {{--{{ session('status') }}--}}
        {{--</div>--}}
        {{--@endif--}}
        {{--<div class="links">--}}
        {{--@foreach ($items as $item)--}}
        {{--                                <br> <a href="{{ $item->name }}">{{ $item->name }}</a>--}}
        {{--<br> <a href="{{ route('items.update', $item->id) }}">{{ $item->name }}</a>--}}

        {{--<br> <img src="{{ $item->image }}" alt="Any alt text"/>--}}
        {{--<p><img src="{{ asset('/gfx/netgate.jpg') }}" /><p>--}}

        {{--@endforeach--}}
        {{--</div>--}}

        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row">--}}
        <div class="d-flex align-content-around flex-wrap">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="row">

                        @foreach($items as $item)
                            <img src="{{ $item->image }}" alt="Any alt text"/>
                            <div class="caption">
                                <h3><a href="{{ route('items.update', $item->id) }}">{{ $item->name }}</a></h3>
                                <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores
                                    error eum inventore officia quis quos totam! Asperiores deleniti, distinctio illum
                                    incidunt nulla officiis sssssssssssssssssssquas suscipit vitae? Magni necessitatibus repellendus
                                    voluptate!</p>
                                <div class="clearfix">
                                    <div class="pull-left price">{{$item->price}}</div>
                                    <a href="{{ route('items.update', $item->id) }}" class="btn btn-success pull-right"
                                       role="button">Details</a>
                                    @endforeach
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

        <h1>Use Bootstrap's carousel to show multiple items per slide.</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide multi-item-carousel" id="theCarousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-xs-4"><a href="#1"><img src="http://placehold.it/300/f44336/000000" class="img-responsive"></a></div>
                        </div>
                        <div class="item">
                            <div class="col-xs-4"><a href="#1"><img src="http://placehold.it/300/e91e63/000000" class="img-responsive"></a></div>
                        </div>
                        <div class="item">
                            <div class="col-xs-4"><a href="#1"><img src="http://placehold.it/300/9c27b0/000000" class="img-responsive"></a></div>
                        </div>
                        <div class="item">
                            <div class="col-xs-4"><a href="#1"><img src="http://placehold.it/300/673ab7/000000" class="img-responsive"></a></div>
                        </div>
                        <div class="item">
                            <div class="col-xs-4"><a href="#1"><img src="http://placehold.it/300/4caf50/000000" class="img-responsive"></a></div>
                        </div>
                        <div class="item">
                            <div class="col-xs-4"><a href="#1"><img src="http://placehold.it/300/8bc34a/000000" class="img-responsive"></a></div>
                        </div>
                        <!-- add  more items here -->
                        <!-- Example item start:  -->

                        <div class="item">
                            <div class="col-xs-4"><a href="#1"><img src="http://placehold.it/300/8bc34a/000000" class="img-responsive"></a></div>
                        </div>

                        <!--  Example item end -->
                    </div>
                    <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                    <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
            </div>
        </div>
    </div>

    </div>
@endsection
