@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="card uper">
        <div class="card-header">
            Edit Item
        </div>
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <form method="post" action="{{ route('items.update', $item->id) }}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">Item Name:</label>
                    <input type="text" class="form-control" name="item_name" value={{ $item->name }} />
                </div>
                <div class="form-group">
                    <label for="name">Item Description:</label>
                    <input type="text" class="form-control" name="item_description" value={{ $item->description }} />
                </div>
                <div class="form-group">
                    <label for="price">Item Price :</label>
                    <input type="text" class="form-control" name="item_price" value={{ $item->price }} />
                </div>
                <div class="form-group">
                    <label for="quantity">Item Quantity:</label>
                    <input type="text" class="form-control" name="item_quantity" value={{ $item->quantity }} />
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
    </div>
@endsection
