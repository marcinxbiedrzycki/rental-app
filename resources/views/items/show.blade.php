@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="container">

    <div class="card border-secondary mb-3" style="max-width: 30rem;">
        <div class="card-header"><center>{{$item->name}}</center></div>
        <div class="card-body text-primary">
            <h5 class="card-title"><img src="{{ $item->image }}" alt="Any alt text"/></h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            <p>{{$item->price}} $</p>
            <button class="btn btn-info" data-toggle="modal" data-target="#order">Rent</button>
            <a href="/" class="card-link">Go back </a>



            <div class="modal fade" id="order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">New Category</h4>
                        </div>
                        <form action="{{route('rents.store')}}" method="post">
                            {{csrf_field()}}
                            <div class="modal-body">
                                @include('items.order')
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>



    </div>
@endsection
