@extends('layouts.app')


@section('content')

<h1>Customer List</h1>
<a href="{{ URL::to('/rents/pdf') }}">Export PDF</a>

<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
    </tr>
    </thead>
    <tbody>
    @foreach($rent as $rents)
        <tr>
            <td>{{ $rents->id }}</td>
            <td>{{ $rents->item->name}}</td>
            <td>{{ $rents->user->name }}</td>

        </tr>
    @endforeach
    </tbody>
</table>

    @endsection
