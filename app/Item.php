<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name',
        'description',
        'price',
        'quantity',
        'image'
    ];


    public function rents()
    {
        return $this->hasMany('\App\Rent');
    }
}
