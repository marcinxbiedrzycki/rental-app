<?php

namespace App\Http\Controllers;

use App\Item;
use App\User;
use App\Rent;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        {
            $rents = Rent::query('rents')
                ->join('items', 'rents.item_id', '=', 'items.id')
                ->join('users', 'rents.user_id', '=', 'users.id' )
                ->where('users.id', '=',Auth::user()->id)
                ->select('rents.*', 'items.id', 'users.id')
                ->get();


            return view('items.rented', compact('rents'));

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rent_quantity' => 'integer|max:2|required',
            'rent_item' => 'required'
        ]);



        $rent = new Rent([
            'quantity' => $request->get('rent_quantity'),
            'user_id' => Auth::user()->id,
            'item_id' => $request->get('rent_item')

        ]);

        $item = Item::where('id', $request->get('rent_item'))->decrement('quantity' , $request->get('rent_item'));

        $rent->save();

        return redirect('/')->with('success', 'You rent an item!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        $rents = DB::table('rents')
            ->join('items', 'rents.item_id', '=', 'items.id')
            ->join('users', 'rents.user_id', '=', 'users.id')
            ->select('rents.*', 'items.id', 'users.price')
            ->get();


        return view('items.rented', compact('rents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




    public function generatePDF()
    {
        $rents = Rent::query('rents')
            ->join('users', 'rents.user_id', '=', 'users.id' )

            ->join('items', 'rents.item_id', '=', 'items.id')
//            ->where('users.id', '=',Auth::user()->id)
            ->select('rents.*')
            ->get();
        $pdf = PDF::loadView('myPDF', ['rents' => $rents]);

        return $pdf->download('allRentedItems' . Carbon::now(). 'pdf');
    }

}
