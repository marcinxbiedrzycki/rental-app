<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    $items = \App\Item::all();
    return view('home', ['items' => $items]);
})->middleware('auth');

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/rents', 'RentController@test')->name('test')->middleware('auth');

Route::get('/admin/rents/generate-pdf','RentController@generatePDF');


Route::resource('rents', 'RentController')->middleware('auth');
Route::resource('items', 'ItemController')->middleware('auth');


Auth::routes();

