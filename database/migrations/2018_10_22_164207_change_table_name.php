<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTableName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->renameColumn('item_name','name');
            $table->renameColumn('item_description','description');
            $table->renameColumn('item_quantity','quantity');
            $table->renameColumn('item_price','price');
            $table->renameColumn('item_image','image');
        });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
            $table->dropColumn('quantity');
            $table->dropColumn('price');
            $table->dropColumn('image');
        });
    }
}
