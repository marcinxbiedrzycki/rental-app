<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameItemTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->renameColumn('name','item_name');
            $table->renameColumn('description','item_description');
            $table->renameColumn('quantity','item_quantity');
            $table->renameColumn('price','item_price');
            $table->renameColumn('image','item_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('item_name');
            $table->dropColumn('item_description');
            $table->dropColumn('item_quantity');
            $table->dropColumn('item_price');
            $table->dropColumn('item_image');
        });
    }
}
