<?php

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'title' => substr($faker->sentence(2),0, -1),
        'description' => substr($faker->sentence(5),0,-1),
        'quantity' => $faker->numberBetween(0,10),
        'price' => $faker->randomFloat(2,1,300),
//        'image' => $faker->image('/resources/gfx',640,480)
        //
    ];
});
